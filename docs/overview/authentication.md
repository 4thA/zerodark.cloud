# Authentication

You have options:

- Bring your own authentication system
- Or use the ZeroDark.cloud identity & authentication system

Many enterprise & established applications will already have an authentication system in place, and that system can easily be integrated with ZeroDark.cloud.

If you're in need of an authentication system, then you're welcome to use the ZeroDark.cloud system. There are no additional fees for doing so.

The ZeroDark identity system is simple, secure & searchable.



---

## Pre-Built User Interface

All client SDK's come with user interface implementations that are ready to be used by your app. So presenting a sign-up/sign-in screen can be accomplished with a few lines of code. 



---

## Multiple Identity Providers

Users can sign into your app using a number of different social identity providers:

- Amazon
- Bitbucket
- Facebook
- Fitbit
- GitHub
- Google
- LinkedIn
- Microsoft
- Salesforce
- Twitter
- WordPress
- Yandex

With more planned & on the way. Plus there's support for:

- Classic username/password

Which is helpful when more anonymity is preferred.



---

## Searchable

Users in the system are searchable using the account(s) they've linked to their identity. Searchability is a critical component for many applications, and is an often overlooked component in many authentication systems.



The client SDK also includes user interface implementations for user searching.



---

## Multiple Linked Identities

Modern users have accounts with multiple social providers. For example, a user may have a Facebook account where they communice with Friends, and a LinkedIn account where the communicate with Colleagues. ZeroDark.identity allows users to link more than one social identity to their account. This makes searching easier - friends will recognize the linked Facebook profile picture, and colleagues will recognize the LinkedIn profile picture.

The client SDK includes a user interface implementation for managing a user's linked identities (adding, removing, changing the default, setting an avatar for username/password accounts, etc).



*Note: A particular social identity (i.e. my [LinkedIn profile](https://www.linkedin.com/in/robbie-hanson-29814228/)) can only be linked to ONE account. It cannot be linked it to multiple accounts. This simplifies searching and increases security.*

