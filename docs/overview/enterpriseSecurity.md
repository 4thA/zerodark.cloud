# Enterprise Security

The stakes for enterprise security have never been higher. The average cost of a data breach was $3.8M in 2018. And this is projected to increase to $150M by the end of 2020.

The number of enterprise attacks has been rapidly increasing. And, on top of that, various industry regulations have threatened large fines for breaches, while mandating only minimal security requirements.

![](img/enterpriseSecurity_risk.jpeg)



## The Flaw: Castle & Moat Security

In the network security world there’s a concept called “**castle and moat**”. The data you store in the cloud is the castle. And the moat includes the various obstacles you put in place to protect the “castle”. You try to make it hard to cross the moat… but once crossed and inside the castle, everyone is trusted by default.

The drawback with this model is it often becomes the vantage point of insider attacks, from both employee/contractor negligence and professional hackers. In fact most security breach studies show around 92% of attacks involve privileged credentials. [[1](https://www.skyhighnetworks.com/cloud-report/)]



The problem stems from the design of cloud permissions, which were modelled after unix permissions. For exampe, you create accounts for employees, and add those accounts to various groups. Then you assign permissions: everyone in group "development" has access to this database. But this system is too broad - as witnessed by the continual news of data breaches worldwide.



ZeroDark.cloud offers a radically different paradigm. It zeros in on what matters most: user data. For each piece of data you store in the cloud, the ZeroDark framework asks: "Who has permission to access this data?"  It then uses client-side encryption to enforce those permissions.



![](img/enterpriseSecurity_recipes.jpeg)



## Need to know

The ZeroDarkCloud framework provides the toolset to achieve need-to-know security. It allows you to bake the security into your applications, and give you programmatic fine-grained-access-control over every piece of user data stored in the cloud. Your application gets to decide:



- which users have permission to read this data
- which servers have permissions to read this data



Those permissions are then enforced using modern cryptography. If only Alice & Bob are granted permission to read a piece of data, then only Alice & Bob have the keys required to decrypt the content. Nobody else.

Finer-grained permissions is a win for security. But ZeroDark.cloud allows you to take your security infrastructure to the next level.



## The question you've never asked yourself

If you're in the business of selling ads, then of course you want to collect as much data on your users as possible. But if you're not Google, then you should ask yourself:



**"Is there any user data I store in the cloud that I don't need to know?"**



Chances are, the answer is "yes". Many applications facilitate various user functionality & collaboration within their app. And the cloud is naturally used in this process to store data. But that means you're putting your business at risk without any chance of reward.



- Are you storing user data in the cloud that you don't need to know?
- What happens if that data is breached by hackers?



With ZeroDark.cloud, you can remove that risk. It allows you to store data in the cloud, without the ability to read it. This removes the burden from your shoulders, and the risk of breaches and fines.