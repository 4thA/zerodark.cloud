# Frequently Asked Questions



###### How do user's transfer private keys to another device ?

If a users wants to login to the same account on another device, they will need to transfer the private key. ZeroDark.cloud client SDK's make this process super easy, by supplying drop-in user interfaces for cloning accounts via:

- a scannable QR-code
  - Simply point a camera, and transfer is complete
- or easy-to-copy / easy-to-type mnemonics
  - The private key is encoded using [BIP-39](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki)



###### How do user's backup their private keys ?

ZeroDark.cloud client SDK's make this process easy by supplying drop-in user interfaces for backing up private keys. Three different options are provided:

###### Social key backup

- The private key is split into parts via a user selected algorithm (e.g. 2-of-3), such that any combination of X parts out of the original Y can be used to restore the key.
- The UI facilitates sending these parts to other people (why it's called "social key backup"), either by utilizing ZeroDark.cloud messaging or by exporting the parts.

###### QR-code

- An easy-to-save image file for backup

###### Text-based mnemonics

- An easy-to-save text-based representation of the file



###### What happens if a user loses their private key ?

User's can login to their account from multiple devices. If they lose one device, and they're logged in on another, they can easily clone their private key from a logged in device. Further, users are encouraged to backup their private key - and the client SDK provides numerous ways to accomplish this. (*See previous question.*)

That being said, if the user loses their private key, and all devices that contained their private key, then they've lost access to their data. If this wasn't true, **it wouldn't be called zero-knowledge**.

As app developers, we realize there's a trade-off here. Stereotypical users want everything. They want a phone that's as thin as possible... and they also want the battery to last 7 days. They want your app to available yesterday... and they also want it to be free of bugs... and infinitely fast... and free... and without ads. You know the drill. And security is no different. Users want infinite privacy and security... and they also want zero responsibility. But the reality is that encryption involves keys, privacy relies on these keys being private, and life requires these keys to be backed up.

ZeroDark.cloud provides the infrastructure to solve these problems in various ways, but YOU determine the tradeoffs that work best for your app. Let's look at some examples:

**The enterprise app**

Enterprise apps have their own regulations to worry about. Using a zero-knowledge sync system built atop AWS is just good business - it means lower prices, better scalability, and less worries about hackers. It means protecting the business. But zero-knowledge in the cloud doesn't mean zero-knowledge for the enterprise. The client has full control over read permissions. Thus most enterprises will provide read-access for the employee's team, or a corporate key for auditability requirements. And in these environments, the enterprise generally holds the private key for the user. 

**The privacy app**

Let's face it - backing up a private key is a pain in the arse. But the industry has already solved similar pains elsewhere! Logging in used to be a pain too - until the industry started adopting "social login", where you can just click a button that says "Login with FooBar". Click once and done - you're in.

Regardless of your feelings about this trend, you should be able to see the abstraction. The user spends time setting up an account once. And from that point forward, the process of logging in becomes much easier. The same is true in ZeroDark.cloud.

If you take advantage of the ZeroDark identity system, users can create an account in app X, and **backup their key ONCE**. Then they can later login to your app (app Y) using the same account - no backup needed. Further, the client SDK's automatically facilitate transferring access. The UI allows the user to launch app X and transfer access to your app:

> Application Y would like permission to access ....

You'll notice this is similar to social login, and is thus familiar to the user. Plus it simplifies the login for the user, while maintaining privacy. And reduces signup/signin friction when onboarding users for your app.

**The custom app**

ZeroDark.cloud is flexible. Tools are provided to solve problems related to sync & messaging, but it's understood that things like identity, authentication & key management are not a one-size-fits all approach. Apps are welcome to provide their own identity & authentication system, and also their own key backup approach. Perhaps you have a custom environment that provides an alternative solution? Great. Bring your own solution, and blaze a trail ! 