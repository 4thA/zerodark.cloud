# Billing

- Part 1 - Overview & explanation
- [Part 2](billing_2.md) - Details & calculations

&nbsp;

## Overview

ZeroDark.cloud utilizes a pure pay-as-you-go billing model. You only pay for server resources consumed.

The prices you pay are set by [AWS](https://aws.amazon.com/pricing/). ZeroDark makes money by adding an additional fee to your bill, which is a percentage of the total. For example, if the monthly total according to AWS is $100, and our fee is 5%, then the total would be $105.

&nbsp;

## Global Deployment

ZeroDark uses a 100% [serverless](https://aws.amazon.com/serverless/) architecture, and the system is designed to be deployed across *all* [AWS regions](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/). As of today, we are deployed in:

- Oregon (us-west-2)
- Ireland (eu-west-1)

As we grow, you should expect this list to increase. (*Also, if you'd like to request a new region, contact us to let us know.*)



You're not restricted to a single region. By design, the system allows individual users to choose which region they'd like to use. So, for example, Alice could setup her account in Oregon, and Bob could setup his account in Ireland, and there would be zero problems communicating between Alice & Bob.



This allows users to choose regions which are geographically closest to them. (The framework recommends the region with the lowest ping response time.) However, you can optionally restrict the region used. This is often done to comply with various regulations. For example, you may need your company data to reside within the EU, and thus you could restrict the regions accordingly.



**AWS has different prices per geographic region**



This is to account for differences in taxes, electricity costs, and other such differences between countries. You may want to take this into consideration when choosing which regions to support. For example, if your application is going to be storage heavy, you may naturally prefer data centers where storage costs are lowest.

&nbsp;

## Partners vs Friends

As discussed in the [co-op](index.md) page, you have 2 options when adopting ZeroDark.cloud:



**1. Developer Partner** — Companies that make apps using ZeroDark.cloud, but which aren't targeting co-op users.

**2. Developer Friend** — Companies that make apps for co-op users. These are apps within the co-op [ecosystem](https://www.zerodark.coop/ecosystem.html) that directly benefit our users & members.



Partners treat us like any other cloud. And they receive a single bill from us at the end of every month.



Friends don't receive a bill from us at the end of the month — they receive a check. Since they make apps for the co-op ecosystem:

- the co-op handles charging users
- the co-op takes zero fees for doing this (as opposed to big tech taking a 30% cut)
- the co-op sends you a check with all your app profits

&nbsp;

## Real-Time Per-User Monitoring

ZeroDark tracks all costs associated with your app. But it takes this a step further. It tracks all costs for each individual user of your app, in real-time.

For example, imagine that Alice is a user of your app. ZeroDark.cloud can tell you, in real-time, exactly how many cloud resources she's consumed. It does this by keeping a running tally of all cloud costs, such as:

- how much cloud storage she's using
- number of push notifications she's triggered
- how many milliseconds of CPU time her account has consumed
- how much bandwidth her account has used



Further, there are API's you can use to query for this information. You can fetch the information, per-user, either for the current month (real-time), or for previous months (archive).

This is useful information that most developers are not accustomed to having. And it opens up new [business possibilities](https://www.zerodark.coop/pay-as-you-go.html).

&nbsp;

#### Next Step

Now you're ready for part 2: [Billing details](billing_2.md)



