# Cloud Architecture

ZeroDark.cloud is built using 100% serverless architecture atop of [AWS](https://aws.amazon.com/) to ensure scalability and provide a pure pay-as-you-go experience.



#### Global Server Fleet

By utilizing AWS, you get access to [global infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/) with 20 different geographic regions across the globe, and at least 4 more coming online soon.

And with ZeroDark.cloud, you don't have to choose between them. The platform allows you to choose which data center to use on a per-user basis. For example, Alice could host her data in Oregon (*us-west-2*), and Bob could host his data in Ireland (*eu-west-1*). And there would be no problem communicating or sharing data between the two users.

Your app can make use of these data centers as needed. For example, enterprises may have legal requirements to host their data in certain geographic regions. Or you can simply optimize by distance or cost.



#### 100% Serverless Freedom

The ZeroDark server codebase is built for the modern cloud, and runs on [AWS Lambda](https://aws.amazon.com/lambda/). This model means you only pay for CPU time that is actually used, in response to user requests. You are now free to use a global infrastructure, knowing there is zero additional cost to do so.



#### 100% S3

Every user in ZeroDark.cloud gets their own bucket in [S3](https://aws.amazon.com/s3/). This provides industry-leading scalability, availability, and performance. And it also means low costs, as ZeroDark.cloud automatically handles transitioning unaccessed files to low-cost tiers such as Infrequent Access & Glacier.

All file downloads are performed directly from S3. No server overhead is required, as read-permissions are handled via [cryptography](encryption_1.md).



#### Push Notifications

All major platforms are supported via [AWS SNS](https://aws.amazon.com/sns/). Users can login to their account on all their devices, and each device will be immediately notified when changes occur that affect their app. If devices are offline, then an optimized fast-forward algorthim is used to quickly bring the app up-to-date.



#### Configurable Hooks

There are a number of hooks you can register for, so that your server is notified of various events as they occur. This includes everything from general user events, to usage threshold events on a per-user basis.

Additionally, the read-permissions of files that get uploaded to the server can optionally include your server, and we'll use a server-side hook to notify you of the event.

&nbsp;
