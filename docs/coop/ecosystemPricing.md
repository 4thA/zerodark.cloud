# Ecosystem Pricing Options



As discussed in the [co-op](index.md) page, you have 2 options when adopting ZeroDark.cloud:



**1. Developer Partner** — Companies that make apps using ZeroDark.cloud, but which aren't targeting co-op users.

**2. Developer Friend** — Companies that make apps for co-op users. These are apps within the co-op [ecosystem](https://www.sync4.cloud/ecosystem.html) that directly benefit our users & members.



We provide several incentives to encourage developers to become friends. And one of these incentives is to provide you with better pricing options for your app.

&nbsp;

## Business models matter

Consider the case of the solar panel industry. Prior to 2008, it seemed everybody wanted the solar panel industry to succeed. But there was a problem — people weren't buying. The upfront cost of the panel was expensive for the average homeowner, and the payoff was years down the road.

But everything changed in 2008 with the introduction of the "zero money down" model. All of a sudden, customers could get a solar panel installed with no upfront costs. So they might pay $75 to lease the panel, and their electricity bill drops by $100. It was a no-brainer, and business exploded.

The right business model for your app can make an oversized difference.

&nbsp;

## Alternative business models

ZeroDark gives you more options for pricing your app.



Traditional Software Business Models:

- Pay once (buy the app)
- Monthly subscription
- Advertising (user is product)



ZeroDark Software Business Models:

- Pay once (buy the app)
- Monthly subscription
- Pay-as-you-go
- Any combination of the above



ZeroDark does NOT charge you fees for handling user payments. And the increased flexibility allows you to create better pricing systems for your app.

&nbsp;

## Example #1 - Monthly Subscription

Say you want to sell an app using a monthly subscription of $1.99. In the traditional case, big tech takes a 20% cut, leaving you with $1.59.



With ZeroDark, you can still choose a monthly subscription of $1.99. In this case, the actual cloud costs are deducted. So, for example, if Alice's cloud costs are $0.20, then she pays $1.99 and you receive $1.79.



Or you can choose to have Alice pay her cloud costs. So Alice would pay $2.19, and then you receive $1.99.

&nbsp;

## Cloud costs

The cloud tracks all costs within the system, and assigns them to the corresponding {user, app} tuple. So the system knows:

- exactly how many push notifications were sent to the user
- how many milliseconds of CPU time were spent processing requests from a specific user
- how many HTTPS requests were processed for the user
- how much cloud storage the user is consuming



At the end of the month this information is tallied. And the price paid by the user is a function of this total, combined with the developer's chosen pricing system.

&nbsp;

## Example #2 - Simple pay-as-you-go

At the end of the month, Bob has racked up $0.50 of cloud costs while using an app. You charge a simple 50% premium over the cloud costs. Bob pays $0.75, and $0.25 goes to you.



The concept of pay-as-you-go is not a new business model. You use it everyday. It's how you pay your electricity bill. It's how you pay for public transportation (metro/bus). It's even how you pay for Uber & electric scooters. But it is new to the world of software.



We have a page dedicated to [explaining pay-as-you-go](https://www.sync4.cloud/pay-as-you-go.html):

- includes a video from a presentation by our CTO
- compares pay-as-you-go to other business models
- explains why pay-as-you-go is so successful
- tells you the secrets for maximizing your profits

&nbsp;

## Example #3 - Simple hybrid

You charge a fixed monthly fee of $1.00, plus a 50% premium over cloud costs. Dave racks up $0.50 of cloud costs while using the app. He pays $1.75, and $1.25 goes to you.

&nbsp;

## Example #4 - Advanced pay-as-you-go

A secure messaging app charges $0.0125 for every message sent, and a 25% premium on cloud storage costs. Chatty Carol pays $2.15 at the end of the month, and $1.65 goes to the developer.

&nbsp;

## Example #5 - Advanced hybrid

A media app charges a fixed monthly fee of $0.25, plus a 25% premium on cloud storage. This allows the company to charge a baseline fee for users, while also adopting a pay-as-you-go model.

&nbsp;

## Win-Win

The co-op works to create a holistic system that benefits both users & developers. Users get a cloud they own & control. And developers get better pricing. Users get dividends as credits in the system. And those credits can only be spent on developer apps.

Plus when developers get better pricing options, everybody wins.



&nbsp;