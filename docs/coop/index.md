# About the co-op



People often ask about the co-op. It's simple:

- ZeroDark.cloud is the tech.
- [ZeroDark.coop](https://www.zerodark.coop/) is the company that owns the tech.



The tech can be used for many purposes, and in many organizations. The co-op welcomes all. But it also has a bigger purpose: to provide data ownership for co-op users.



As a developer, you have 2 options when adopting ZeroDark.cloud:



**1. Developer Partner** — Companies that make apps using ZeroDark.cloud, but which aren't targeting co-op users.

**2. Developer Friend** — Companies that make apps for co-op users. These are apps within the co-op [ecosystem](https://www.zerodark.coop/ecosystem.html) that directly benefit our users & members.

&nbsp;

## Developer Partners

Our partners treat us like any other cloud infrastructure. That is:

- you provide your own user login system
- you handle customer billing & payments
- and we send you a bill at the end of the month for the cloud resources consumed



You can think of ZeroDark.cloud as an encryption layer which sits between your app and AWS. There's an S3 bucket for each user, and the framework handles encrypting & syncing data to the bucket.



The system is pure pay-as-you-go, so you pay only for the AWS resources used.

&nbsp;

## Developer Friends

Our friends take advantage of our app ecosystem:

- the framework provides the user login system
- the co-op handles customer billing & payments for you
- we send you a check at the end of each month from customer payments
- the co-op distributes profits to the members in the form of credits, which can only be spent on apps within the ecosystem (like yours)



The co-op has a grand vision and difficult mission. It aims to give its users ownership & control of their digital life — which includes their app data. And it aims to restore their digital privacy too. But to effect change we need many stakeholders. That's why the company is a co-op. We're creating a holistic system for users & developers to work together — to create a win-win outcome.

&nbsp;

## Ecosystem Incentives

So as a developer, what's in it for you? Why would you become a Developer Friend, and join the [ecosystem](https://www.zerodark.coop/ecosystem.html)?



#### Lower Fees

When you sell your app through Big Tech they take a Big Cut — up to 30% of your revenue. That's brutal. Especially for Indie developers that don't have a million dollar advertising budget.

When you sell your app through the co-op ecosystem, we take No Cut — a 0% fee. Nada. Nothing. Zip. Zilch.

Remember, the co-op is user-owned. If we charge you a fee for handling payments, you're just going to transfer those fees to the user. So let's just drop it.



#### Better Pricing Options

Within the ZeroDark ecosystem, you get [better pricing options](ecosystemPricing.md) for your app.



#### User incentives

The co-op offers users incentives to sign-up. We give them [ownership & control](https://www.sync4.cloud/) of their digital life. And the co-op offers them the option of becoming a member, and receiving dividends from company profits.

