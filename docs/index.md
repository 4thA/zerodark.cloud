# ZeroDark.cloud™

A zero-knowledge sync & messaging framework for your app, built atop AWS & the Ethereum blockchain.



[https://www.zerodark.cloud](https://www.zerodark.cloud)

&nbsp;

---

## The Docs

The documentation is split into 4 main sections to make it easier to find what you're looking for.

#### Overview

Here you'll find high-level information discussing how the system works, how the architecture is setup, and various articles that explain the encryption, blockchain integration, authentication, etc.

- [Overview documentation](overview/index.md)

#### Cooperative

ZeroDark is owned by a cooperative. Find out how this benefits you, and how you can make more money by switching to our ecosystem.

- [Co-op documentation](coop/index.md)

#### Client-Side Developer

Ready to dive into some code? The client-side developer documentation jumps into the open-source framework, and discusses how to use it, and how it works.

- [Client-Side Developer documentation](client/index.md)

#### Server-Side Integration

Ready to leverage the power of AWS, and take your app to the next level? ZeroDark.cloud comes with several server-side hooks you can use expand the functionality of your app.

- [Server-Side Integration documentation](server/index.md)

