# Client Setup - Part 3

- Part 1 - [Install the framework](setup_1.md)
- Part 2 - [Initialize the framework](setup_2.md)
- Part 3 - User login & signup screens
- Part 4 - [Register app in the dashboard](setup_4.md)
- Part 5 - [Configure push notifications](setup_5.md)



## User login & signup screens

To store data in the cloud we need a logged in user. So the next step is the user login & signup flow.



You have 2 options here:

- you can build your own custom authentication system, or
- you can use the pre-built system that comes with the framework



If you provide your own custom authentication system, you're choosing to be a [developer partner](../coop/index). If you use the pre-built system you're choosing to be a [developer friend](../coop/index).



If you're new to the framework, we recommend starting with our pre-built authentication system. This allows you to get up-and-running quickly. And chances are you'll want to test drive ZeroDark before committing to a big project.



#### Pre-Built Authentication

You can display the pre-built authentication flow with just a few line of code.

```swift
func showAuth() {
  
	let setupVC = zdc.uiTools.accountSetupViewController(
	                           withInitialViewController: nil,
	                         canDismissWithoutNewAccount: true)
	{(localUserID: String?, completedActivation: Bool, shouldBackupAccessKey:Bool) in

   //	do something with new localUserID
	}

	// display the setupVC
}
```

*Tip: To see a working example, check out the [WhatsZapp](https://github.com/4th-ATechnologies/ZeroDark.cloud/tree/master/Samples/WhatsZapp) sample app.*



This displays the UI that allows a user to either create a new account, or login to an existing account.

The closure is invoked when the user has either completed or cancelled the authentication flow. If completed, a `localUserID` variable is returned, which is needed to perform most activities within the framework.

And with your logged in user, you're almost ready to start [pushing](./push) data to the cloud. But first, you need to register your app container with ZeroDark.



#### Bring Your Own Authentication

If you're looking to be a developer partner, you'll be providing your own authentication. So there are a few extra hoops to jump thru. Checkout the [partners](partners.md) page for more information.



#### Next Step

Now you're ready for part 4: [Register app in the dashboard](setup_4.md)

