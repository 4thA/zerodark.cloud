# Client-Side Development

You can find the GitHub repos here:

- [iOS & macOS](https://github.com/4th-ATechnologies/ZeroDark.cloud)

---

## Sample Code

The repository comes with a few sample apps. Each sample app comes with a README file which explains how it works. When additional documentation is needed, it links back to here.

###### [ZeroDarkTodo](https://github.com/4th-ATechnologies/ZeroDark.cloud/tree/master/Samples/ZeroDarkTodo)

A Todo app is your standard "Hello World" when it comes to syncing data. This app demonstrates the basics. It also allows you to share a Todo list with others users, thus demonstrating how easy it is to add collaboration features to your app.

###### [WhatsZapp](https://github.com/4th-ATechnologies/ZeroDark.cloud/tree/master/Samples/WhatsZapp)

A simple messaging app. And since it's built atop ZeroDark, this means all messages flowing through the server are encrypted end-to-end. The server is not capable of reading any of the messages. It used to be really hard to make such an app. ZeroDark makes it easy.



---

## Getting Started

- Get [setup](setup_1.md) with ZeroDark.cloud for your app
- Learn about the [treesystem](tree.md)
- How to [push](push.md) data up to the cloud
- How to [pull](pull.md) data down from the cloud
- How to send & receive [messages](messaging.md)



---

## Tutorials

Once you understand the basics, use the tutorials to walk through the development process:

- Walk through the process of [downloading](downloadTutorial), from start to finish
- Learn about [mapping](mappingTutorial.md) from nodes to your custom objects



---

## Tools of the Trade

The framework is split into multiple classes, based on functionality. Learn about the tools at your disposal.

- The [DownloadManager](downloadManager.md) handles all of your downloading needs
- The [DiskManager](diskManager.md) assists you in caching & persisting files to disk
- The [ImageManager](imageManager.md) simplifies the process of fetching, manipulating & displaying images
- The [NodeManager](nodeManager.md) provides various tools to explore the treesystem
- The [DatabaseKeyManager](databaseKeyManager.md) provides a powerful & secure key management system
- The [ProgressManager](progressManager.md) tells you what network operations are in-flight



---

## Advanced

- [Merging changes](merging.md) made easy

- Understanding the [push queue](pushQueue.md)

  

