# Client Setup - Part 1

- Part 1 - Install the framework
- Part 2 - [Initialize the framework](setup_2.md)
- Part 3 - [User login & signup screens](setup_3.md)
- Part 4 - [Register app in the dashboard](setup_4.md)
- Part 5 - [Configure push notifications](setup_5.md)



## Welcome

The official reporitory is: [github.com/4th-ATechnologies/ZeroDark.cloud](https://www.github.com/4th-ATechnologies/ZeroDark.cloud)



The repository comes with a few sample apps. It's easy to test drive these sample apps, so you can test out the framework and its features.



## Install the framework

To start using ZeroDarkCloud in your own application, your first step is to install the framework using CocoaPods.



#### CocoaPods

To install with [CocoaPods](https://cocoapods.org/), just add the following to your Podfile:

```
pod 'ZeroDarkCloud/Swift'
```

And then run `pod install` as ususal.



#### Test Installation

Test that the framework was installed properly by importing it within your code, and ensuring you don't get any compiler errors. Just add the following import somewhere (e.g. to your AppDelegate):

```swift
// Swift import
import ZeroDarkCloud
```

```objective-c
// Objective-C import
#import <ZeroDarkCloud/ZeroDarkCloud.h>
```



#### Next Step

Now you're ready for part 2: [Initialize the framework](setup_2.md)

