# Client Setup - Part 2

- Part 1 - [Install the framework](setup_1.md)
- Part 2 - Initialize the framework
- Part 3 - [User login & signup screens](setup_3.md)
- Part 4 - [Register app in the dashboard](setup_4.md)
- Part 5 - [Configure push notifications](setup_5.md)



## Initialize the framework

To initialize the framework, you just create an instance of [ZeroDarkCloud](https://apis.zerodark.cloud/Classes/ZeroDarkCloud.html). This instance is responsible for communicating between your device and the cloud.

#### Name your app container

Every app gets its own cloud container. So you just need to come up with a unique name for your app's cloud container. To prevent conflicts, we recommend using a reverse DNS name. For example: `com.businessName.appName`.



(*Within the code, this is referred to as the treeID, which is a reference to the [treesystem](tree.md) – you'll learn about this shortly.*)



#### Intialize during app setup

```swift
import ZeroDarkCloud

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ZeroDarkCloudDelegate {
  
  func setupZDC() {

    let treeID = "com.myBusinessNameHere.myAppNameHere"
    let config = ZDCConfig(primaryTreeID: treeID)

    zdc = ZeroDarkCloud(delegate: self, config: config)
    do {
      let dbEncryptionKey = try zdc.databaseKeyManager.unlockUsingKeychain()
      let dbConfig = ZDCDatabaseConfig(encryptionKey: dbEncryptionKey)

      zdc.unlockOrCreateDatabase(dbConfig)
    } catch {
      print("Something went wrong: \(error)")
    }
    
    // zdc instance is now running & ready for use !
  }

  // MARK: ZeroDarkCloudDelegate protocol

  // Stub in the functions required to implement
  // the ZeroDarkCloudDelegate protocol here.
  // 
  // Xcode can do this for you automatically.
  
  func data(for node: ZDCNode, at path: ZDCTreesystemPath, transaction: YapDatabaseReadTransaction) -> ZDCData {
    return ZDCData()
  }
  
  // Other protocol stubs here...
}
```



#### Notes

- You generally only need a single zdc instance. One instance is capable of supporting multiple users.

- ZeroDarkCloud stores state in a local database. For security, this database is encrypted.



#### Next Step

Now you're ready for part 3: [User login & signup screens](setup_3.md)

