# Client Setup - Part 4

- Part 1 - [Install the framework](setup_1.md)
- Part 2 - [Initialize the framework](setup_2.md)
- Part 3 - [User login & signup screens](setup_3.md)
- Part 4 - Register app in the dashboard
- Part 5 - [Configure push notifications](setup_5.md)



## Register app in the dashboard

In [part 2](setup_2.md) you picked a unique name for your app's cloud container. This was something like `com.businessName.appName`. Now we're going to register that name online so that it belongs to you.



Go to [dashboard.zerodark.cloud](https://dashboard.zerodark.cloud), and create an account using a username & password. After doing so, you can navigate to the "Apps" section, and then "Create new app":

![Screenshot](img/dashboardApps.png)

You'll need to perform this step before you can start uploading data to the cloud. Otherwise all uploads with fail with the error `UnknownTreeID`.



#### Next Step

There's only one step left: [Configuring push notifications](setup_5.md)



However, if you're anxious to get started, your app is ready to sync with the cloud now. So you can skip ahead to [pushing](push.md) data up to the cloud, and [pulling](pull.md) data down from the cloud. Just keep in mind that push notifications aren't going to work until you complete [part 5](setup_5.md). In other words, if you're logged into the same account on device A & B, and you modify the cloud data on device A, then device B won't instantly update until you [configure push notifications](setup_5.md).