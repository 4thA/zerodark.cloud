# ProgressManager

[ZDCProgressManager](https://4th-atechnologies.github.io/ZeroDark.cloud/Classes/ZDCProgressManager.html) tracks all networking operations being performed by the library. This includes uploads & downloads.

For each operation, an NSProgress is available. Further, the ProgressManager injects real-time information into the NSProgress userInfo dictionary, including:

- estimated time remaining
- throughput (bandwidth)



---

## Downloads

You can use the [DownloadManager](/client/downloadManager) to download any node, which returns a [ZDCDownloadTicket](https://4th-atechnologies.github.io/ZeroDark.cloud/Classes/ZDCDownloadTicket.html) result. This ticket contains the NSProgress. But you can also fetch the same NSProgress instance from the ProgressManager at any time:

```swift
let progress = zdc.progressManager.dataDownloadProgress(forNodeID: nodeID)
```

The ProgressManager also injects real-time status info for you:

```swift
let remaining = progress.userInfo[.estimatedTimeRemainingKey]
let throughput = progress.userInfo[.throughputKey]
```

In addition to fetching the progress, you can also add a completion listener:

```swift
zdc.progressManager.addDataDownloadListener(forNodeID: nodeID, completionQueue: DispatchQueue.main) {(cloudDataInfo, cryptoFile, error) in
  // Download finished
}
```

For metadata downloads, there's a similar API:

```swift
let progress = zdc.progressManager.metaDownloadProgress(forNodeID: nodeID)
```



---

## Uploads

You can fetch the upload progress for any active operation in the [push queue](/client/pushQueue). Most of the time, however, you're primarily interested in the upload of your app's content (i.e. the data component of the upload). This is easy to fetch:

```swift
let progress = zdc.progressManager.dataUploadProgress(forNodeID: nodeID)
```

