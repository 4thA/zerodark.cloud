# Database Key Manager 

ZDC uses strong encryption to protect the database on the device, and one of the first things your app will need to do before using ZDC is to provide  the database with an 256 bit encryption key.  Since encryption  is only as secure as the key management,  we make this very easy for you get this right by giving you some tools to protect and easily access this database encryption key.

##### ZDC database key management options 

ZDC gives you a few options for managing the database key and also allows you to switch back and forth given your user's security needs.

- Store the key on the iOS/macOS keychain. 
- Require user biometric / fingerprint to unlock the key.
- Encrypt the key to a user passcode.

You can even optionaly opt-out and manage the key yourself.  While we don't recommend this path beacuse it is often hard to get right, we recognize that some developers have specific key management needs and give you the option.

##### Unlock using the Keychain.

Both iOS and macOS offer the Keychain,  a built in secure database to protect keys.   On modern  IOS devices and the MacBook Pro with the Touch Bar the keychain is backed by a hardware-based key manager known as the Secure Enclave that’s isolated from the main processor. This is very resistant to outside attacks.   Both iOS and macOS keychain are backed with solid cryptography that has undergone extensive scrutiny such as FIPS 140-2 validation.

Accessing keychain  database encyption key from your app is fairly easy and only requires one API call:

```swift
let dbEncryptionKey = try zdc.databaseKeyManager.unlockUsingKeychainKey()
```

##### Unlock using the Biometrics / Fingerprint.

Optionally you might chose to protect your app using the device fingerprint reader if its available. Since might vary from device to device,  we give you a device independent API that we can expand in the future as new methods become available.

The first thing you will want to do is to check if your device has biometric  capabilities:

~~~~swift
let hasBiometrics = zdc.databaseKeyManager.canUseBioMetrics
~~~~

The next step is to  create the biometric key:

~~~~ swift
try zdc.databaseKeyManager.createBiometricKey()
~~~~

Once a key is available.  You can opt to unlock with that key.

~~~swift
// check for biometric encryption
	if(zdc.databaseKeyManager.usesBioMetrics){

		// attempt to unlock
    let dbEncryptionKey 
				= try zdc.databaseKeyManager.unlockUsingBiometricKey(withPrompt:"Unlock this app")
  }
~~~

If you decide to later can remove the biometric key easily:

~~~~swift
	try zdc.databaseKeyManager.removeBioMetricKey()
~~~~

If you plan to use FaceID biometric authentication to unlock the database you will also need to add the following key to your application Info.plist file.

- "Privacy: Face ID Usage Description" == NSFaceIDUsageDescription 

##### Unlock with a Passcode

Another alternative and often used as a backup to  biometrics is to protect the database with a user supplied passcode.  Your app is responsible for prompting the user for a passphrase and an optional hint.  You then pass this information to  ZDC with the following API:

~~~~~   swift
	try zdc.databaseKeyManager.createPassPhraseKey(passPhrase:"your passcode",
                                                 passPhraseHint: "hint")()
~~~~~

Once a passphrase protect key is created, you can unlock with that key:

~~~~ swift
// check for passphrase encryption
if(zdc.databaseKeyManager.usesPassphrase)
{
  // is there a passphrase hint
  let hint = zdc.databaseKeyManager.hintStringForPassphraseKey()
  
  //prompt user for passphase ->
  let passphrase = myCodeTogetPassphrase()
  
  // attempt to unlock
	let dbEncryptionKey
  			= try zdc.databaseKeyManager.unlock(usingPassphaseKey: passphrase)
}
~~~~

You can remove the passphrase and fallback to to keychain protected key with the following API call:

```swift
	try zdc.databaseKeyManager.removePassphraseKey()
```

