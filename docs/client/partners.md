# Partners

As described in the [co-op](../coop/index) page, you have two options when adopting ZeroDark:



**1. Developer Partner** — Companies that make apps using ZeroDark.cloud, but which aren't targeting co-op users.

**2. Developer Friend** — Companies that make apps for co-op users. These are apps within the co-op [ecosystem](https://www.zerodark.coop/ecosystem.html) that directly benefit our users & members.



If you choose to be a partner, you have a few additional tasks to perform.



## Authentication

Partners provide their own authentication system. That is, you're not using the authentication system that comes with the framework.

However, in order for your users to talk to the cloud, you need to generate a refreshToken for the user.



#### Create a refreshToken for your server

Go to [dashboard.zerodark.cloud](https://dashboard.zerodark.cloud), and login to your account. Then navigate to the "Servers" section, and click on "Create a server token":

![Screenshot](img/dashboard_tokens1.png)

You're prompted for a name. This is just an optional human-friendly name, and can be whatever you want. For example, "Server #1". Next click the "Create" button, and you'll be prompted to save your token.

![Screenshot](img/dashboard_tokens2.png)

Make sure you save the token by copy-n-pasting it somewhere. Because once you click the "Done" button the system won't ever display the token again (for security reasons).

![Screenshot](img/dashboard_tokens3.png)



#### Fetch an accessToken

ZeroDark follows the best security practices, and here we're mirroring the oAuth v2 protocol. So there are separate refreshTokens & accessTokens.



- A refreshToken is used to request an accessToken. A refreshToken never expires, although you have the power to revoke it (from the dashboard).

- An accessToken is used to communicate with the ZeroDark API. An accessToken expires after a short period of time, generally around 4 hours.



ZeroDark uses JSON Web Tokens (JWT) as accessTokens. For more information about JWT's, check out [jwt.io](https://jwt.io/).



To get an access token, just issue a request like so:

```sh
curl --request POST \
  --url https://xx08iqr297.execute-api.us-west-2.amazonaws.com/v1/public/auth/jwt \
  --header 'Content-Type: application/json' \
  --data '{"biz_id":"your_biz_id_here","token":"your_refreshToken_here"}'
```

*You can get your business id (biz_id) from the dashboard's [company](https://dashboard.zerodark.cloud/company) page.*



The response is JSON:

```json
{
  "jwt": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFmOTk1NzJkLWQ2Y2UtNDE2Zi04MWFlLTljMzkwNjZiZjk4OCJ9.eyJpYXQiOjE1ODc1MjQzNTUsImV4cCI6MTU4NzUzODc1NSwiYXVkIjoiYTVjOWMwZDhlNzI1NDFkMzhhNzE5ZGU4MTY1OWI5MTkiLCJpc3MiOiJodHRwczovL3Jlc291cmNlcy56ZXJvZGFyay5jb29wLyIsInN1YiI6ImZqYzdjb2M3eGdvamRrMzd4dzl1ZzVldCJ9.FRm7WO_7TZhDSYLilAYGwCu0h-4JtordQ-kv75sMYV0MJaVPIFcCCGLhDksiA8mOUj8hbZlea_bjgnu_IcOVvZFfi0yamne-A6Xs8KW21l13K8movu41vmPLOmYrs2DlgqDHTAFJ1l1XBqnlAlD5W_28PUTT5TCbfu9Ein1J_r-lH9N_3CuSXHSU0ls4McS-pt2A8DhWYX0wbkKHJTSPq8TGYhM1dz90Gikmtlf8-oPLBrlMnoQLdFEwcRCgAgm9DgDX5fhSavzzZTJDavLZnAcDBxxLre-yI2hf6We3noJ9TZJRMdJpOFS5KFk7DUi3NFDOutsf5YsMtrDMImoH8A"
}
```

*Tip: Try pasting your JWT into [jwt.io](https://jwt.io/) to see what it contains.*



#### Create a user in ZeroDark

Now that you have a JWT, you're ready to access the full ZeroDark API. The next step is to create a user within ZeroDark.



Partners manage their own authentication. So the ZeroDark user is just a shadow of the user within your own system. And ZeroDark wants to avoid any identifying information about your user. Thus there are only 2 things you need to create a shadow user:

- **user_id** — a 160-bit value that you provide
- **aws region** — geographic region in which to store the user's data



The user_id might just be a random value that you generate:

```javascript
// generating a random 160-bit value in node.js:
const crypto = require('crypto')
const buffer = crypto.randomBytes(160/8)
const user_id_hex = buffer.toString('hex')

// user_id_hex = 'bd4b74524c2fc2d354666d545f897641306943c3'
```

Alternatively, you might hash something unique about your user, and then take the first 160 bits of the hash.



The AWS region is one of:

- "us-west-2" (Oregon)
- "eu-west-1" (Ireland)



Then just issue a request like so:

```shell
curl --request POST \
  --url https://xx08iqr297.execute-api.us-west-2.amazonaws.com/v1/authdBizSrv/users/create \
  --header 'Content-Type: application/json' \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFmOTk1NzJkLWQ2Y2UtNDE2Zi04MWFlLTljMzkwNjZiZjk4OCJ9.eyJpYXQiOjE1ODc1MjQzNTUsImV4cCI6MTU4NzUzODc1NSwiYXVkIjoiYTVjOWMwZDhlNzI1NDFkMzhhNzE5ZGU4MTY1OWI5MTkiLCJpc3MiOiJodHRwczovL3Jlc291cmNlcy56ZXJvZGFyay5jb29wLyIsInN1YiI6ImZqYzdjb2M3eGdvamRrMzd4dzl1ZzVldCJ9.FRm7WO_7TZhDSYLilAYGwCu0h-4JtordQ-kv75sMYV0MJaVPIFcCCGLhDksiA8mOUj8hbZlea_bjgnu_IcOVvZFfi0yamne-A6Xs8KW21l13K8movu41vmPLOmYrs2DlgqDHTAFJ1l1XBqnlAlD5W_28PUTT5TCbfu9Ein1J_r-lH9N_3CuSXHSU0ls4McS-pt2A8DhWYX0wbkKHJTSPq8TGYhM1dz90Gikmtlf8-oPLBrlMnoQLdFEwcRCgAgm9DgDX5fhSavzzZTJDavLZnAcDBxxLre-yI2hf6We3noJ9TZJRMdJpOFS5KFk7DUi3NFDOutsf5YsMtrDMImoH8A' \
  --data '{"user_id_hex":"bd4b74524c2fc2d354666d545f897641306943c3","region":"us-west-2"}'
```

Make sure you replace 3 things from the above:

- JWT
- user_id_hex
- desired region



The response is JSON:

```JSON
{
  "status": 201,
  "user_id": "zifzew1cf9bpgidgpikf9nmserag1o6d",
  "bucket": "com.4th-a.user.zifzew1cf9bpgidgpikf9nmserag1o6d-4e098f17",
  "region": "us-west-2",
  "stage": "prod",
  "salt": "9E6642D43E43410DA2903CA62AFA1671",
  "created": 1587407877295
}
```



*Tip: ZeroDark stores user_id's in zBase32. So the same 160 bits can be represented in hexadecimal as `bd4b74524c2fc2d354666d545f897641306943c3` or in zBase32 as `zifzew1cf9bpgidgpikf9nmserag1o6d`.*



#### Create a refreshToken for the user

The last step is to create the refreshToken:

```shell
curl --request POST \
  --url https://j1n0wvoo16.execute-api.us-west-2.amazonaws.com/v1/authdBizSrv/users/createRefreshToken \
  --header 'Content-Type: application/json' \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFmOTk1NzJkLWQ2Y2UtNDE2Zi04MWFlLTljMzkwNjZiZjk4OCJ9.eyJpYXQiOjE1ODc1MjQzNTUsImV4cCI6MTU4NzUzODc1NSwiYXVkIjoiYTVjOWMwZDhlNzI1NDFkMzhhNzE5ZGU4MTY1OWI5MTkiLCJpc3MiOiJodHRwczovL3Jlc291cmNlcy56ZXJvZGFyay5jb29wLyIsInN1YiI6ImZqYzdjb2M3eGdvamRrMzd4dzl1ZzVldCJ9.FRm7WO_7TZhDSYLilAYGwCu0h-4JtordQ-kv75sMYV0MJaVPIFcCCGLhDksiA8mOUj8hbZlea_bjgnu_IcOVvZFfi0yamne-A6Xs8KW21l13K8movu41vmPLOmYrs2DlgqDHTAFJ1l1XBqnlAlD5W_28PUTT5TCbfu9Ein1J_r-lH9N_3CuSXHSU0ls4McS-pt2A8DhWYX0wbkKHJTSPq8TGYhM1dz90Gikmtlf8-oPLBrlMnoQLdFEwcRCgAgm9DgDX5fhSavzzZTJDavLZnAcDBxxLre-yI2hf6We3noJ9TZJRMdJpOFS5KFk7DUi3NFDOutsf5YsMtrDMImoH8A' \
  --data '{"user_id_hex":"bd4b74524c2fc2d354666d545f897641306943c3","context":"Whatever you want here, e.g. iPhone 8"}'
```



The response is JSON:

```json
{"token":"EB6D22FB5128406D83E842AC42688846"}
```



This is a refreshToken that's valid for the user. The user will be able to use this refreshToken in a similar way. That is, they'll exchange it for a JWT. And the JWT will allow them to talk to the user-related APIs.



*Note: There are more APIs for managing refreshTokens. Find out by downloading our OpenAPI v3 specification here.*



#### Pass info to the user

*Section coming soon*

