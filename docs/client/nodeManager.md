# NodeManager

ZDCNodeManager makes it easy to navigate the treesystem.

&nbsp;

---

#### Node <-> TreesystemPath

Every node is a part of the [treesystem](/client/tree.md). So a common task you may need to perform is fetching the treesystem path for a given node. The NodeManager provides an easy solution:

```swift
databaseConnection.read {(transaction) in
  let path = zdc.nodeManager.path(for: node, transaction: transaction)
}
```

The inverse task is similar:

```swift
databaseConnection.read {(transaction) in
  let node = zdc.nodeManager.findNode(with: path, localUserID: localUserID, zAppID: zAppID, transaction: transaction)
}
```

&nbsp;

---

## Containers

As discussed in the [treesystem](/client/tree.md) article, there are various "root level" containers. The most common is the Home container. You can fetch these containers like so:

```swift
databaseConnection.read {(transaction) in
  let home = zdc.nodeManager.containerNode(forLocalUserID: localUserID, zAppID: zAppID, container: .home, transaction: transaction)
}
```

&nbsp;

---

## Enumerating the Treesystem

You can enumerate the nodes in the treesystem in various ways. To enumerate the direct children of a particular node:

```swift
databaseConnection.read {(transaction) in
  zdc.nodeManager.enumerateNodes(withParentID: parentID, transaction: transaction) {(node, stop) in
    // access child node here
  }
}
```

You can also perform the enumeration in a recursive manner, which will also give you the grandchildren & futher descendants. For example, here's how you could enumerate every single node in the home container:

```swift
databaseConnection.read {(transaction) in
  let home = zdc.nodeManager.containerNode(forLocalUserID: localUserID, zAppID: zAppID, container: .home, transaction: transaction)!
  zdc.nodeManager.recursiveEnumerateNodes(withParentID: home, transaction: transaction) {(node, parents, recurseInto, stop) in
    // access descendent node here
  }
}
```

