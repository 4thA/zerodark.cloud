# Cryptocurrency Wallet

ZeroDark.cloud could be used to to support a cryptocurrency wallet. It comes with several features a wallet needs, and the zero-knowledge encryption ensures that senstive information remains private.



---

#### Notifications

How does a wallet know when it's received a payment? If the wallet provider wishes to support this feature in real-time, then push notifications will need to be used to notify the app.

ZeroDark.cloud can make this process quite easy and secure. Every user in the system has an inbox - a per-app folder for receiving messages. Uploading a file into this folder (a simple HTTPS operation), will automatically send a push notification to the users devices.

Furthermore, the information in the file will be encrypted such that only the intended recipient can decrypt it. This is accomplished using standard encryption routines, and a javascript implementation is already available. For details, see the "Implementation Details - Notifications" section below.



---

#### Easy P2P Payments

Cryptocurrency wallets have to compete with modern P2P payment apps, like Square Cash. And they usually fall short because they fail the most basic P2P test.

If Alice wants to send a payment to Bob, then she needs to be able to get payment information from Bob. For example, a Bitcoin address. Most cryptocurrency wallet designers imagine that Alice & Bob will both take out their phones at a restaurant, and nerdily scan a QR-code at the table. The problem is, that's not what actually happens in real life:

- Alice paid for Bob because Bob left his wallet & phone in his other jacket
- Alice & Bob were at a noisy bar, and it was easier to put both drinks on one tab
- A million other reason why Alice & Bob didn't dutifully scan QR-codes

The solution that actually works is:

- Alice can SEARCH for Bob, and find the information she needs to send him a payment

There are various reasons the early cryptocurrency community has generally resisted this obvious solution. However, this will be a requirement for a cryptocurrency wallet to go mainstream. The question is:

**How can a cryptocurrency wallet make payment details searchable, while still maintaining privacy ?**

The answer is surprisingly simple, but it requires 2 things:

1. A way to search for other users
2. A way to securely communicate between those 2 users

Here's how it works:

- Bob makes himself searchable, by linking some kind of information to his account. This could be something like his Facebook account. Or it could be something more anonymous, like a made up username.
- Alice is able to search for Bob, and find him.
- Alice's wallet sends a secure message to Bob's wallet, asking for payment details. This message is sent securely - only Bob is able to decrypt the message, and Bob is able to verify it came from Alice.
- Bob's wallet can generate a unique address specifically for Alice.
- Bob's wallet securely sends this information back to Alice - again, only Alice is able to decrypt the message, and Alice is able to verify it came from Bob.

You now have searchability, without revealing any addresses to the world.

ZeroDark.cloud makes all of this possible. It's a secure messaging platform. And it even comes with an identity system, that allows Bob to link social identities to his account. This includes Facebook, LinkedIn, etc - but it also allows him to create a username too. And, ZeroDark.cloud comes with user search, built on these linked identities.



---

#### Key Backup

Cryptocurrency wallets face the same problem that all privacy apps face - the user is resposible for backing up a key. ZeroDark.cloud comes with extensive key backup tools - pre-built user interfaces that can be used to backup any kind of keys. These tools can be fed with cryptocurrency keys, and the user will be given several backup options:

###### Social Key Backup

- Splits the key into several pieces via a user-selected algorithm (e.g. 2-of-3)
- Allows the user to send these pieces to other people for safe keeping
- The user interface for recombining the keys is already done for you too
- And because ZeroDark.cloud is a secure messaging platform too, the process of actually sending the pieces can be performed securely. (Otherwise, you know the user is going to send all those pieces using Gmail...)

###### QR-code Backup

- The user can choose to backup the key as a QR-code
- This works well if the user wishes to save the file, or print it

###### Text-based Mnemonic Backup

- Text is always an easy backup solution
- Easy to write down, or easy to store in a password manager



It's a lot of work to create all of these tools from scratch. You get them for free with the ZeroDark client SDK.



---

#### Implementation Details - Notifications

Every user in the ZeroDark.cloud system has a public key. For example, here's the public key of a ZeroDark engineer - [Robbie Hanson's public key](https://s3-us-west-2.amazonaws.com/com.4th-a.user.z55tqmfr9kix1p1gntotqpwkacpuoyno-e0975e8e/.pubKey) (it's a JSON file).

The public key can be verified using the [Ethereum smart contract](/overview/ethereum.md).

To send this user a message, we'd start with the following JSON skeleton:

```JSON
{
    "data": "... see instructions below ..."
    "keys": {
        "UID:z55tqmfr9kix1p1gntotqpwkacpuoyno": {
            "perms": "rws",
            "key": "... see instructions below ..."
        }
    }
    "version": 3
}
```

The userID (z55tqm....) is Robbie Hanson's userID. You would replace this with the userID of the desired recipient.

Next, create a (cleartext) message to send to the user. This can be whatever you want. We're going to encrypt it momentarily:

```JSON
{
    "type": "incoming-payment",
    "addr": "1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2",
    "amount": "0.12345"
}
```

Next, generate a random 512 bit key - called the file encryption key. Use Threefish-512 to encrypt the cleartext using the file encryption key. Then encode the ciphertext using Base64, and inject it into the JSON file in the "data" section. It will look something like this:

```JSON
{
    "data": "zL9FiZFl5Pbo4NXQkrWanA6yWI0B4QTdYSKISnqAsKfX38Kwd2/KNE0bOTysRk9wXq0XKswWPaxEtv0OVuf4MLaZubQTQPTvCt4k4J1/K9Et1pWuYZjOS7uKvZrwQ9rsVLNh9Ne6lJjeQ5Rny+CrI4HM6pI="
    "keys": {
        "UID:z55tqmfr9kix1p1gntotqpwkacpuoyno": {
            "perms": "rws",
            "key": "... see instructions below ..."
        }
    }
    "version": 3
}
```

Next, wrap the file encryption key with the user's public key. This means the user's private key will be required to unwrap the file encryption key. And inject it into the JSON file in the "key" section. It will look something like this:

```JSON
{
    "data": "zL9FiZFl5Pbo4NXQkrWanA6yWI0B4QTdYSKISnqAsKfX38Kwd2/KNE0bOTysRk9wXq0XKswWPaxEtv0OVuf4MLaZubQTQPTvCt4k4J1/K9Et1pWuYZjOS7uKvZrwQ9rsVLNh9Ne6lJjeQ5Rny+CrI4HM6pI="
    "keys": {
        "UID:z55tqmfr9kix1p1gntotqpwkacpuoyno": {
            "perms": "rws",
            "key": "eyJ2ZXJzaW9uIjoxLCJlbmNvZGluZyI6IkN1cnZlNDE0MTciLCJrZXlJRCI6InlLcVl3U2dzL2drUTRSTVlsME9wYVE9PSIsImtleVN1aXRlIjoiVGhyZWVGaXNoLTUxMiIsIm1hYyI6IjV4ZEZCMEprNjVvPSIsImVuY3J5cHRlZCI6Ik1JSEVCZ2xnaGtnQlpRTUVBZ01FZFRCekF3SUhBQUlCTkFJMEdxSDN3Q3hNdDRWaGp6MS9aaFZrbmk4WGtQWTFUMFp4SlZrZytJZEgra1pBNHl1bFZadER2cC9yUlIvUlRtRUZ0K2JjUEFJMElFenFMeVA5RmpoNXRmZnNxa1AraDNSNlVuZmxpZnVDQVgyU291L0R1eGRncFU0L2RyelNkN0lrZUp3WGV1Z0dXNWhBM1FSQUQrYkhSTFhtbzk3eTB3Y1MrOUtmUnZ5dm55M1MxYUNlZmRzNWk4M1lEWE8rbW00WmdpSU1TbDhncnM2andPMVVOajQwZjZ4MW9ZVC9pTStzMUdRc3hRPT0ifQ=="
        }
    }
    "version": 3
}
```

This file can then be uploaded to the user's bucket. That will trigger a push notification, and the ZeroDark framework can automatically download & decrypt the file.

There is an [open source project](https://github.com/4th-ATechnologies/users.storm4.cloud) that demonstrates sending files and messages to ZeroDark.cloud users. We will be adding more example projects soon. (Simpler ones, that are smaller & easier to read!)